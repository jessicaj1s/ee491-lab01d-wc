///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Jessica Jones <jjones2@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   29_Jan_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void info();

int main(int argc, char * argv[]) {

  //Command line parameter processing
  if (argc != 2) {
    info();
    exit(EXIT_FAILURE);
  }

  if ( argc == 2 ) {
    FILE * file;
    int numOfChar = 0;
    int numOfLines = 0;
    int numOfWords = 0;
    int wordMode = 1;
    char chCurrent = '\0';
    char chPrevious = '\0';

    //File management logic
    // Open file in read only mode
    file = fopen(argv[1], "r");

    //Check if file opened successfully, if not exit
    if ( file == NULL ) {
      fprintf(stdout, "%s: Can't open [%s]\r\n", argv[0], argv[1]);
      exit(EXIT_FAILURE);
    }

    //Word counting logic
    // If file opened successfully, then write the string to file

    while ( ( chCurrent = fgetc(file) ) != EOF ) {

      //line ends in \r or \n
      if ( chCurrent == '\r' || chCurrent == '\n' ) {
        numOfLines++;
      }

      //avoid double counding \r\n 
      if ( chPrevious == '\r' && chCurrent == '\n' ) {
        numOfLines--;
      }

      //words end in \n, \t, or space; the start of a word is a character and previous is whitespace 

      //if chCurrent is a character and chPrevious is whitespace
      if ( (isgraph(chCurrent) && (!isspace(chCurrent)) ) && isspace(chPrevious) ) {
        wordMode = 1;
      }


      //if word mode is on, chCurrent is whitespace, and chPrevious is ASCII, you have reached the end of the word
      if ( wordMode == 1 && isspace(chCurrent) && (isgraph(chPrevious) && !isspace(chPrevious) ) ) {
	    wordMode = 0; 
        numOfWords++;
      }

      //char are anything that isn't whitespace
	  //check unicode
      if ( (!iscntrl(chCurrent)) && (!isspace(chCurrent)) && (isprint(chCurrent)) || ( (unsigned char)chCurrent >= 0xC0 ) ) {
		numOfChar++;
      }
	  
	  chPrevious = chCurrent;
	  
    }
    //Presentation logic — printing the results out		 
    printf( "%d\t%d\t%d\t%s\r\n", numOfLines, numOfWords, numOfChar, argv[1] );
    fclose(file);
	
    return 0;
  }
}

void info() {
  fprintf( stdout, "Usage:  wc FILE\r\n" );
}
